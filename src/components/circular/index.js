import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Circle from 'react-native-progress-circle';

const circular = () => {
  return (
    <View style={styles.container}>
      <Circle
        percent={75}
        radius={50}
        borderWidth={3}
        color="#1A2BD1"
        bgColor="#fff">
        <Text style={styles.textOne}>Ran</Text>
        <Text style={styles.textTwo}>5.1</Text>
        <Text style={styles.textOne}>Miles</Text>
      </Circle>

      <Circle
        percent={75}
        radius={50}
        borderWidth={3}
        color="#1A2BD1"
        bgColor="#fff">
        <Text style={styles.textOne}>Pace</Text>
        <Text style={styles.textTwo}>10.8</Text>
        <Text style={styles.textOne}>Min/mi</Text>
      </Circle>

      <Circle
        percent={75}
        radius={50}
        borderWidth={3}
        color="#1A2BD1"
        bgColor="#fff">
        <Text style={styles.textOne}>Max Hr</Text>
        <Text style={styles.textTwo}>144</Text>
        <Text style={styles.textOne}>Bpm</Text>
      </Circle>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 22,
    paddingHorizontal: 18,
  },
  textOne: {
    color: '#A1A1A1',
    fontSize: 12,
  },
  textTwo: {
    color: '#A1A1A1',
    fontSize: 21,
    fontWeight: '700',
  },
});

export default circular;
