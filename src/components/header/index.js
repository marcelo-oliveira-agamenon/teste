import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

import logo from '../../../assets/grupo-73.png';
import chat from '../../../assets/chat_icon.png';
import avatar from '../../../assets/94.jpg';

const header = () => {
  return (
    <View style={styles.container}>
      <Image source={avatar} style={styles.avatar} />

      <Image source={logo} />

      <Image source={chat} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 13,
    borderRadius: 18,
    height: 60,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },
  avatar: {
    width: 35,
    height: 35,
    borderRadius: 30,
  },
});

export default header;
