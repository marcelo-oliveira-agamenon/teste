import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  ScrollView,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import {LineChart, Grid} from 'react-native-svg-charts';
import MapView from 'react-native-maps';
import Geo from 'react-native-geolocation-service';

import Header from '../../components/header';
import Circle from '../../components/circular';
import layer from '../../../assets/layer-2.png';
import run from '../../../assets/running.png';
import sets from '../../../assets/text1.png';
import clock from '../../../assets/clock.png';
import run1 from '../../../assets/text.png';
import fitbit from '../../../assets/fitbit-282220.png';
import bicicle from '../../../assets/invalid-name.png';
import car from '../../../assets/noun-driving-2750433.png';

const home = () => {
  const [location, setLocation] = useState({});

  useEffect(() => {
    getPermissions();
    Geo.getCurrentPosition(
      (position) => {
        setLocation({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        });
      },
      (error) => {
        console.log(error);
      },
      {
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 10000,
      },
    );
  }, []);

  async function getPermissions() {
    if (Platform.OS === 'ios') {
      Geo.requestAuthorization();
    } else if (Platform.OS === 'android') {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
    }
  }

  const data1 = [50, 40, 43, 48, 50, 40, 58, 66, 50, 45, 48, 55, 60, 40, 35];
  const data2 = [48, 50, 51, 55, 58, 45, 50, 48, 44, 52, 55, 50, 44, 41, 50];

  const data = [
    {
      data: data1,
      svg: {stroke: 'purple'},
    },
    {
      data: data2,
      svg: {stroke: 'green'},
    },
  ];

  return (
    <ScrollView>
      <Header />
      <View style={styles.type}>
        <View style={styles.inside}>
          <Image source={layer} />
          <Image source={run} style={styles.textInside} />
        </View>

        <View style={styles.type2}>
          <Text style={styles.connectedText}>Connected</Text>
          <Image source={fitbit} style={styles.imgFit} />
        </View>
      </View>

      <Circle />

      <View style={styles.boxCount}>
        <View style={styles.iconCont}>
          <View>
            <Image source={sets} style={styles.imgCont} />
            <Text style={styles.textCron}>10 kms</Text>
          </View>

          <View>
            <Image source={clock} style={styles.imgCont} />
            <Text style={styles.textCron}>38min</Text>
          </View>

          <View>
            <Image source={run1} style={styles.imgCont} />
            <Text style={styles.textCron}>4:50/KM</Text>
          </View>
        </View>

        <View style={styles.divider}></View>

        <View>
          <View style={styles.textResume}>
            <Text style={styles.textColorResume}>Last run</Text>

            <Text style={styles.textColorResume}>12km - 48 min - 4:50/KM</Text>
          </View>

          <LineChart
            style={{height: 200, paddingHorizontal: 18}}
            data={data}
            contentInset={{top: 40, bottom: 40}}>
            <Grid direction={Grid.Direction.VERTICAL} />
          </LineChart>
        </View>
      </View>

      <View style={styles.mapCont}>
        <View style={styles.contMap}>
          <Text>Trips and travels</Text>

          <View style={styles.type2}>
            <Text style={styles.connectedText}>Connected</Text>
            <Image source={fitbit} style={styles.imgFit} />
          </View>
        </View>

        <MapView
          style={styles.map}
          region={{
            latitude: 40.68152983464134,
            longitude: -73.9371420954751,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          showsUserLocation
        />

        <View style={styles.footerMap}>
          <View>
            <Image source={bicicle} style={styles.imgMapCont1} />
            <Text style={styles.textMapIcon}>RUNNING</Text>
            <Text style={styles.textMapIcon}>8.6 hours</Text>
          </View>

          <View>
            <Image source={bicicle} style={styles.imgMapCont2} />
            <Text style={styles.textMapIcon}>BICYCLE</Text>
            <Text style={styles.textMapIcon}>1.1 hours</Text>
          </View>

          <View>
            <Image source={car} style={styles.imgMapCont3} />
            <Text style={styles.textMapIcon}>TRAVEL</Text>
            <Text style={styles.textMapIcon}>39 minutes</Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  type: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginTop: 12,
    paddingVertical: 13,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    opacity: 0.5,
  },
  imgFit: {
    width: 58,
    height: 22,
    marginLeft: 12,
  },
  contMap: {
    zIndex: 8,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginTop: 15,
  },
  textCron: {
    textAlign: 'center',
    fontWeight: '700',
    marginTop: 5,
  },
  textInside: {
    marginLeft: 8,
  },
  type2: {
    display: 'flex',
    flexDirection: 'row',
  },
  inside: {
    display: 'flex',
    flexDirection: 'row',
  },
  connectedText: {
    color: '#3B8501',
    paddingVertical: 3,
    borderColor: '#3B8501',
    borderRadius: 26,
    borderWidth: 1,
    paddingHorizontal: 13,
    fontSize: 12,
  },
  boxCount: {
    marginHorizontal: 13,
    borderRadius: 18,
    height: 'auto',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },
  iconCont: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: 18,
  },
  divider: {
    paddingTop: 10,
    paddingBottom: 18,
    marginHorizontal: 15,
    borderBottomWidth: 0.8,
    borderBottomColor: '#E9E9E9',
  },
  imgCont: {
    width: 55,
    height: 55,
  },
  textResume: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 22,
    paddingTop: 12,
  },
  textColorResume: {
    color: '#C8C6C6',
  },
  mapCont: {
    height: 450,
    marginTop: 22,
    marginBottom: 50,
    borderRadius: 12,
    marginHorizontal: 13,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },
  footerMap: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 300,
  },
  imgMapCont1: {
    backgroundColor: '#47DED8',
    borderRadius: 60,
    marginBottom: 5,
    alignSelf: 'center',
    height: 30,
    width: 30,
  },
  imgMapCont2: {
    backgroundColor: '#3F4CC5',
    borderRadius: 60,
    marginBottom: 5,
    alignSelf: 'center',
    height: 30,
    width: 30,
  },
  imgMapCont3: {
    backgroundColor: '#7EA8D9',
    borderRadius: 60,
    marginBottom: 5,
    alignSelf: 'center',
    height: 30,
    width: 30,
  },
  textMapIcon: {
    color: '#000',
    marginTop: 2,
    fontWeight: '700',
  },
});

export default home;
